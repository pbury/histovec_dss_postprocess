from datetime import datetime
import configparser
import json


def date_to_year(date_mise_en_service: str) -> int:
    try:
        date_split = date_mise_en_service.split('/')
        year_mise_en_service = int(date_split[2])
    except IndexError:
        year_mise_en_service = datetime.now().year + 1
    return year_mise_en_service


def main(genre, ptac, usages=[], categorie=None, date_mise_en_service=None):
    config = configparser.ConfigParser()
    config.read('post_process.ini')
    annee_mise_en_service_mini = int(config['UTAC']['annee_mise_en_service_mini'])
    ptac_max = int(config['UTAC']['ptac_max'])
    genres_ct = json.loads(config['UTAC']['genres_for_ct_json'])
    usages_ct = json.loads(config['UTAC']['usages_for_ct_json'])
    categories_ct = json.loads(config['UTAC']['categories_for_ct_json'])

    if categorie == 'None':
        categorie = None

    if date_mise_en_service == 'None':
        date_mise_en_service = None

    ptac = int(ptac)

    check_utac_for_ct = False
    reason = "Défault"

    if date_mise_en_service is None:
        check_utac_for_ct = True
        reason = "Pas de date de mise en service"

    annee_mise_en_service = date_to_year(date_mise_en_service=date_mise_en_service)

    if categorie is not None:
        if categorie in categories_ct:
            check_utac_for_ct = True
            reason = "categorie dans liste"
        if usages in usages_ct and annee_mise_en_service >= annee_mise_en_service_mini and ptac <= ptac_max:
            check_utac_for_ct = True
            reason = " categorie et usage = COL and mise en service ok et ptac ok"

    if categorie is None:
        if genre in genres_ct:
            check_utac_for_ct = True
            reason = "Pas categorie et genre dans liste"
        if usages in usages_ct and annee_mise_en_service >= annee_mise_en_service_mini and ptac <= ptac_max:
            check_utac_for_ct = True
            reason = "Pas categorie et usage = COL and mise en service ok et ptac ok"

    return check_utac_for_ct, "%s \n genre %s \nptac %s \nusages %s\n categorie %s\n date %s" % \
           (reason, genre, ptac, usages, categorie, date_mise_en_service)


if __name__ == '__main__':
    print(main(genre='VP', ptac='3000', usages=[], categorie='M1', date_mise_en_service='1212/1999'))
