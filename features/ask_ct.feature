Feature: Ask UTAC for CT data

  Scenario Outline: with categories
     Given we have a vehicle with <genre> <categorie> <ptac> <date_mise_en_service> <usages>
      Then result should be <ask_ct>


  Examples: with_categories
    | genre | categorie | ptac | date_mise_en_service | usages | ask_ct |
    | VP | M1 | 3000 | 10/10/2010 | None | True |
    | VP | C2 | 3000 | 18/1/2011 | None | False |
    | VP | C3 | 3000 | 1/1/2010 | COL | True |
    | VP | XXXC3 | 3000 | 2/1/1955 | COL | False |

  Examples: without_categories
    | genre | categorie | ptac | date_mise_en_service | usages | ask_ct |
    | VP | None | 3000 | 1/1/2010 | None | True |
    | MAGA | None | 3000 | 2/1/2010 | None | False |
    | VP | None | 3000 | 3/12/2010 | COL | True |
    | VP | None | 3600 | 4/11/2010 | COL | True |
    | VP | None | 3400 | 2010 | COL | True |