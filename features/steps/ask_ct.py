from behave import *
import check_utac


@given('we have a vehicle with {genre} {categorie} {ptac} {date_mise_en_service} {usages}')
def step_impl(context, genre, categorie, ptac, date_mise_en_service, usages):
    (context.response, context.reason) = check_utac.main(genre=genre, categorie=categorie, ptac=int(ptac), date_mise_en_service=date_mise_en_service, usages=usages)



@then('result should be {ask_ct}')
def step_impl(context, ask_ct):
    str_response = str(context.response)
    print(context.reason)
    assert ask_ct == str_response

