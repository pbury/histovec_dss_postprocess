// Rajouter ask_ct dans ES
// ida1, ida2, idv, v, utac_id, (ask_ct)


// Traduction data V -> variables du script
// ----------------------------------
// const genre = CTEC_RLIB_GENRE
// const categorie = CTEC_RLIB_CATEGORIE
// const ptac = ptac_f2
// const dateMiseEnService = date_premiere_immat || MISSING_VALUE
// const usages = usage || []


import { CATEGORIE } from './constants/vehicle/categorie'
import { GENRE } from './constants/vehicle/genre'
import { USAGE } from './constants/vehicle/usage'


const extractYear = (dateString, defaultYear) => {
	const [, , year] = dateString.split('/')
	return year ? year : defaultYear
}

export const isSubjectToTechnicalControl = ({ categorie, dateMiseEnService, genre, ptac, usages=[] }) => {
	// If no dateMiseEnService, correlated condition must always returns true to ask UTAC
	const defaultYear = new Date().getFullYear() + 1
	const anneeMiseEnService = extractYear(dateMiseEnService, defaultYear)

	if (
		categorie &&
		([
			CATEGORIE.M1,
			CATEGORIE.M1G,
			CATEGORIE.M2,
			CATEGORIE.M2G,
			CATEGORIE.M3,
			CATEGORIE.M3G,
			CATEGORIE.N1,
			CATEGORIE.N1G,
			CATEGORIE.N2,
			CATEGORIE.N2G,
			CATEGORIE.N3,
			CATEGORIE.N3G,
			CATEGORIE.O3,
			CATEGORIE.O4,
		].includes(categorie) ||
			(usages.includes(USAGE.COL) && anneeMiseEnService >= 1960 && ptac <= 3500)
		)
	) {
		return true
	}

	if (
		!categorie &&
		([
			GENRE.VP,
			GENRE.CTTE,
			GENRE.VASP,
			GENRE.TCP,
			GENRE.CAM,
			GENRE.TRR,
			GENRE.REM,
			GENRE.RESP,
			GENRE.RETC,
			GENRE.SREM,
			GENRE.SRSP,
			GENRE.SRAT,
			GENRE.SRTC,
		].includes(genre) ||
			(usages.includes(USAGE.COL) && anneeMiseEnService >= 1960 && ptac <= 3500)
		)
	) {
		return true
	}

	return false
}


// const askCt = isSubjectToTechnicalControl({ categorie, dateMiseEnService, genre, ptac, usages })
// => à mettre dans ES