from check_utac import main as check_utac_main
import dataiku
import pandas as pd, numpy as np
from dataiku import pandasutils as pdu
import configparser

config = configparser.ConfigParser()
config.read('post_process.ini')


# ---------------------------------
#
# UTAC
#
# ---------------------------------
utac_input_dataset = int(config['UTAC']['input_dataset'])
utac_output_dataset = int(config['UTAC']['output_dataset'])
utac_genre_column_idx = int(config['UTAC']['genre_column_idx'])
utac_ptac_column_idx = int(config['UTAC']['ptac_column_idx'])
utac_usages_column_idx = int(config['UTAC']['usages_column_idx'])
utac_categorie_column_idx = int(config['UTAC']['categorie_column_idx'])
utac_date_mise_en_service_column_idx = int(config['UTAC']['date_mise_en_service_column_idx'])

# Recipe inputs
utac_input = dataiku.Dataset(utac_input_dataset)
utac_input_df = utac_input.get_dataframe()

# utac_input_df['utac_ct'] = utac_input_df.apply(lambda x: check_utac_main(genre=x[utac_genre_column_idx], ptac = x[utac_ptac_column_idx], usages=x[utac_usages_column_idx], categorie=x[utac_categorie_column_idx],date_mise_en_service=x[utac_date_mise_en_service_column_idx],  axis=1)
# FROM print(check_utac_main(genre='VP', ptac=, usages=[], categorie='M1', date_mise_en_service='1212/1999'))

# Recipe outputs
output_dataset = dataiku.Dataset(utac_output_dataset)
output_dataset.write_with_schema(utac_input_df)

